package main;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Provide a text that needs to be reversed:");
        String textInput = scanner.nextLine();

        CustomStringUtils customStringUtils = new CustomStringUtils();

        System.out.println(customStringUtils.getTextReversed(textInput));
    }
}