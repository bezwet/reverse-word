package main;

public class CustomStringUtils {

    public String getTextReversed(String text) {
        char[] chars = text.toCharArray();
        int tailPointer = chars.length -1;
        int headPointer = 0;
        while (headPointer < tailPointer) {
            char tempChar = chars[tailPointer];
            chars[tailPointer] = chars[headPointer];
            chars[headPointer] = tempChar;
            headPointer++;
            tailPointer--;
        }
        return new String(chars);
    }
}
