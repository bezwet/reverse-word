package test;

import main.CustomStringUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomStringUtilsTests {

    @ParameterizedTest(name = "Word to be reversed: {0}. Reversed word: {1}")
    @CsvSource({"kayak,kayak", "java, avaj", "lombok, kobmol", "object oriented programming, " + "gnimmargorp detneiro tcejbo", "java17, 71avaj"})
    public void getTextReversed_test(String word, String result) {
        CustomStringUtils customStringUtils = new CustomStringUtils();
        String reversedWord = customStringUtils.getTextReversed(word);

        assertEquals(result, reversedWord);
    }
}
